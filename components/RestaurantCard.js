import { View, Text, TouchableOpacity, Image } from "react-native";
import React from "react";
import Icon from "react-native-vector-icons/Ionicons";
import { urlFor } from "../sanity";
import { useNavigation } from "@react-navigation/native";

const RestaurantCard = ({
  id,
  imgUrl,
  title,
  rating,
  genre,
  address,
  shortDesc,
  dishes,
  long,
  lat,
}) => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate("Restaurant", {
          id,
          imgUrl,
          title,
          rating,
          genre,
          address,
          shortDesc,
          dishes,
          long,
          lat,
        });
      }}
      className="bg-white mr-3"
    >
      <Image
        source={{
          uri: urlFor(imgUrl).width(200).url(),
        }}
        className="h-36 w-64 rounded-sm"
      />
      <View className="pb-4 px-3">
        <Text className="font-bold text-lg pt-2">{title}</Text>
        <View className="flex-row items-center space-x-1">
          <Icon name="star" color="green" size={22} />
          <Text className="text-gray-500">
            <Text className="font-bold">{rating}</Text> · {genre}
          </Text>
        </View>

        <View className="flex-row items-center space-x-1 mt-1">
          <Icon name="location-outline" color="#8c8c8c" size={22} />
          <Text className="text-gray-500 text-xs">Nearby · {address}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default RestaurantCard;
