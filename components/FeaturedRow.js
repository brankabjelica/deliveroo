import { View, Text, ScrollView } from "react-native";
import React, { useEffect, useState } from "react";
import Icon from "react-native-vector-icons/Feather";
import RestaurantCard from "./RestaurantCard";

import sanityClient, { urlFor } from "../sanity";

const FeaturedRow = ({ id, title, description }) => {
  const [restaurants, setRestaurants] = useState([]);

  useEffect(() => {
    sanityClient
      .fetch(
        `*[_type == "featured" && _id == $id] {
          ...,
          restaurants[]->{
            ...,
            dishes[]->,
              type->{
                name
              }
          },
        }[0]`,
        { id }
      )
      .then((data) => {
        setRestaurants(data?.restaurants);
      });
  }, [id]);

  //console.log(restaurants);

  return (
    <View>
      <View className="mt-4 flex-row items-center justify-between px-4">
        <Text className="font-bold text-lg">{title}</Text>
        <Icon name="arrow-right" color="#77CCBB" size={24} />
      </View>

      <Text className="text-xs text-gray-500 px-4">{description}</Text>

      <ScrollView
        horizontal
        contentContainerStyle={{
          paddingHorizontal: 15,
        }}
        showsHorizontalScrollIndicator={false}
        className="pt-4"
      >
        {/* Restaurant Cards*/}

        {/* <RestaurantCard
          id={1}
          imgUrl="https://imageproxy.wolt.com/menu/menu-images/6273d9a14f0302a85b5356ad/d428eb34-cc7d-11ec-9bdf-fe9168ad3535_burgeri_bacon_double_burger.jpeg"
          title="boorgir"
          rating={4.98}
          genre="boorgir"
          address="Goranova 2"
          shortDesc="boorgir"
          dishes={["boorgir 1", "boorgir 2", "boorgir 3"]}
          long={45.12}
          lat={2.18}
        /> */}

        {restaurants?.map((restaurant) => {
          console.log(restaurant.name);
          <RestaurantCard
            key={restaurant._id}
            id={restaurant._id}
            imgUrl={restaurant.image}
            address={restaurant.address}
            title={restaurant.name}
            dishes={restaurant.dishes}
            rating={restaurant.rating}
            shortDesc={restaurant.short_description}
            genre={restaurant.type?.name}
            long={restaurant.long}
            lat={restaurant.lat}
          />;
        })}
      </ScrollView>
    </View>
  );
};

export default FeaturedRow;
