import { useNavigation } from "@react-navigation/native";
import React, { useEffect, useLayoutEffect, useState } from "react";

import {
  View,
  Text,
  StatusBar,
  Image,
  TextInput,
  ScrollView,
  Platform,
} from "react-native";

import Icon from "react-native-vector-icons/Feather";
import Categories from "../components/Categories";
import FeaturedRow from "../components/FeaturedRow";
import sanityClient from "../sanity";

const HomeScreen = () => {
  const navigation = useNavigation();
  const [featuredCategories, setFeaturedCategories] = useState([]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, []);

  useEffect(() => {
    sanityClient
      .fetch(
        `*[_type == "featured"] {
          ...,
          restaurants[]->{
              ...,
              dishes[]->           
          }
        }`
      )
      .then((data) => {
        setFeaturedCategories(data);
      });
  }, []);

  return (
    <View
      style={{
        paddingTop: Platform.OS === "android" ? StatusBar.currentHeight + 5 : 0,
      }}
      className="bg-white mb-6"
    >
      {/* Header */}
      <View className="flex-row pb-3 items-center mx-4 space-x-2.5">
        <Image
          source={{ uri: "https://links.papareact.com/wru" }}
          className="h-7 w-7 bg-gray-300 p-4 rounded-full"
        />

        <View className="flex-1">
          <Text className="font-bold text-gray-400 text-xs">Deliver now!</Text>
          <Text className="font-bold text-xl">
            Current Location{" "}
            <Icon name="chevron-down" size={20} color="#77CCBB" />
          </Text>
        </View>
        <Icon name="user" size={36} />
      </View>

      {/* Search */}
      <View className="flex-row items-center space-x-2 pb-2 px-4">
        <View className="flex-row bg-gray-200 flex-1 space-x-3 p-2 ">
          <Icon name="search" color="gray" size={25} />
          <TextInput
            placeholder="Restaurants and cuisines"
            keyboardType="default"
          />
        </View>
        <Icon name="filter" color="#77CCBB" size={25} />
      </View>

      {/* Body */}
      <ScrollView
        className="bg-gray-100"
        contentContainerStyle={{
          paddingBottom: 100,
        }}
      >
        <View>
          {/* Categories  */}
          <Categories />

          {/* Featured Rows */}
          {featuredCategories?.map((category) => (
            <FeaturedRow
              key={category._id}
              id={category._id}
              title={category.name}
              description={category.short_description}
            />
          ))}
        </View>
      </ScrollView>
    </View>
  );
};

export default HomeScreen;
