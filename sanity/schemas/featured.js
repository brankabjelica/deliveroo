export default {
  name: "featured",
  title: "Odabrano",
  type: "document",
  fields: [
    {
      name: "name",
      type: "string",
      title: "Odabrane Kategorije",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "short_description",
      type: "string",
      title: "Kratak opis",
      validation: (Rule) => Rule.max(200),
    },
    {
      name: "restaurants",
      type: "array",
      title: "Restorani",
      of: [{ type: "reference", to: [{ type: "restaurant" }] }],
    },
  ],
};
