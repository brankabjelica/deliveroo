export default {
  name: "category",
  title: "Kategorija",
  type: "document",
  fields: [
    {
      name: "name",
      type: "string",
      title: "Kategorija",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "image",
      type: "image",
      title: "Slika Kategorije",
    },
  ],
};
