export default {
  name: "dish",
  title: "Jelo",
  type: "document",
  fields: [
    {
      name: "name",
      type: "string",
      title: "Jelo",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "short_description",
      type: "string",
      title: "Kratak opis",
      validation: (Rule) => Rule.max(200),
    },
    {
      name: "price",
      type: "number",
      title: "Cena u dinarima (RSD)",
    },
    {
      name: "image",
      type: "image",
      title: "Slika Jela",
    },
  ],
};
