export default {
  name: "restaurant",
  title: "Restoran",
  type: "document",
  fields: [
    {
      name: "name",
      type: "string",
      title: "Restoran",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "short_description",
      type: "string",
      title: "Opis Restorana",
      validation: (Rule) => Rule.max(200),
    },
    {
      name: "image",
      type: "image",
      title: "Slika Restorana",
    },
    {
      name: "lat",
      type: "number",
      title: "Kooordinate (lat.)",
    },
    {
      name: "long",
      type: "number",
      title: "Kooordinate (long.)",
    },
    {
      name: "address",
      type: "string",
      title: "Adresa Restorana",
      validation: (Rule) => Rule.required(),
    },
    {
      name: "rating",
      type: "number",
      title: "Prosečna ocena restorana: ",
      validation: (Rule) =>
        Rule.required()
          .min(1)
          .max(5)
          .error("Molimo unesite vrednost izmedju 1 i 5"),
    },

    {
      name: "type",
      title: "Kategorija",
      validation: (Rule) => Rule.required(),
      type: "reference",
      to: [{ type: "category" }],
    },
    {
      name: "dishes",
      type: "array",
      title: "Jela",
      of: [{ type: "reference", to: [{ type: "dish" }] }],
    },
  ],
};
